import json
import requests
from keras.datasets import mnist


def argmax(lst):
    return lst.index(max(lst))

if __name__ == "__main__":
    # Note that the mnist data can be loaded in many different ways. this is only one way.
    (train_data, train_labels), (test_data, test_labels) = mnist.load_data()

    # Reshape the data as single data-points and normalize
    test_data = test_data.reshape(
                test_data.shape[0], 1, 28, 28, 1
            )
    test_data = test_data.astype("float32")
    test_data /= 255

    # Change this if you want to infer any of the other data points
    # This can be, in this case (using keras), a number from 0 to 10000

    data_point = 0

    dp = test_data[data_point]
    label = test_labels[data_point]

    payload = json.dumps(
        {"instances": dp.tolist()}
    )
    response = requests.post(
        url=f"http://localhost:8080/invocations",
        data=payload,
        headers={"Content-Type": "application/json"},
    )

    response = response.json()
    predictions = response['predictions'][0]


    for digit in range(10):
        print(f"{digit}: {round(predictions[digit] * 100, 2)}%")

    predicted = argmax(predictions)

    print(f"Prediction: {predicted}")
    print(f"True Label: {label}")
