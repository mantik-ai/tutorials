# Prediction on MNIST dataset

> NOTE: Until Download model everything can be done in the frontend as well

## Register Model

> NOTE:  for now `uri` and `location` have to be added in the CLI command even if they are not needed in this case

```commandline
mantik models add --name "My-trained-model" --project-id <your project id> --run-id <your run id> --uri "Does not matter if Run id is passed" --location "Does not matter if Run id is passed"
```

## Containerize trained model
```commandline
mantik models build --project-id <your project id> --model-id <your model id>
```

## Check build status

```commandline
mantik models build-status --project-id <your project id> --model-id <your model id>
```

When status is successful the model has been containerized.

## Download Model

```commandline
mantik models download --project-id <your project id> --model-id <your model id> --load
```
> NOTE: With `load` the model is already in your docker images

## Run model container

> NOTE: If you downloaded the container with `no-load` or from the frontend you might need to run:
> 
> `docker load < <your model id>-docker.tar.gz`

```commandline
docker run -p 8080:8080 <your model id>-docker
```

## Predict

Set up python environment:
```commandline
python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt
```

Run predict script:
```commandline
python3 predict.py
```

> NOTE: Feel free to change the `predict.py` file to suit your needs