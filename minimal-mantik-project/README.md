# Minimal Mantik Projcet

**Project author:** Fabian Emmerich

**Mantik version:** 0.4.2

This repository contains a minimal [Mantik](https://cloud.mantik.ai) project.

The project is located in the `mlproject` folder and contains a single entry point that `echo`s the input.

> **Note:**
> You can follow this tutorial on the mantik docs [here](https://mantik-ai.gitlab.io/mantik/tutorials/mantik-minimal-project/tutorial-outline.html).

## Prerequisites

You will need:
 - a Mantik user account (register [here](https://cloud.mantik.ai/register))
 - a Mantik project with an experiment (see [here](https://mantik-ai.gitlab.io/mantik/ui/quickstart.html))

## Run Project

### Set up environment variables for the Unicore Trial Instance

Authentication to [UNICORE Trial Instance](https://unicore.dev2.cloud.mantik.ai/):

```commandline
export MANTIK_UNICORE_USERNAME=demouser
export MANTIK_UNICORE_PASSWORD=test123
export MANTIK_COMPUTE_BUDGET_ACCOUNT=whatever
```

Additionally, the information on where to send MLflow logs to is required:

```commandline
export MLFLOW_TRACKING_URI=https://trackingserver.cloud.mantik.ai/
```

Provide your mantik credentials

```commandline
export MANTIK_USERNAME=<user>
export MANTIK_PASSWORD=<password>
```
### Submit run

Get your MLflow experiment ID from the [experiment tab](https://mantik-ai.gitlab.io/mantik/projects_ui/experiments.html), and run the following command.

```bash
mantik runs submit mlproject/MLproject \
  --run-name "mantik-minimal-project from CI" \
  --connection-id <Mantik Connection UUID> \
  --entry-point main \
  --backend-config compute-backend-config.yaml \
  --project-id <ID of the project to which the run should be linked> \
  --experiment-repository-id <ID of the experiment repository to which the run should be linked> \
  --code-repository-id <ID of the code repository where the mlproject is located> \
  --branch <Name of the code repository's branch> \
  --compute-budget-account <Name of the compute budget account on HPC> \
  -P experiment_name="Test mantik minimal project"
```