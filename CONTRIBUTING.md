# Contributing to mantik tutorials

## Table of Content

1. [Governance](#governance)
2. [How To Contribute](#how-to-contribute)
   1. [Reporting bugs](#reporting-bugs)
   2. [Suggesting improvements](#suggesting-improvements)
3. [Code contributions](#code-contributions)
   1. [Adding an example](#adding-an-example)
   2. [Merge Requests](#merge-requests)
4. [Contributors](#contributors)

## Governance

Governance of mantik is conducted by [ambrosys GmbH](https://ambrosys.de) represented by

* [Fabian Emmerich](https://gitlab.com/fabian.emmerich) [(fabian.emmerich@4-cast.de)](mailto:fabian.emmerich@4-cast.de)
* [Thomas Seidler](https://gitlab.com/thomas_ambrosys) [(thomas.seidler@ambrosys.de)](mailto:thomas.seidler@ambrosys.de)

## How to Contribute

### Reporting bugs

This section guides you through submitting a bug report for the mantik tutorials.
Following these guidelines helps understand your report, reproduce the behavior, and find related reports.

**Notes:**
* Before submitting a bug report, check that your **issue does not already exist** in the [issue tracker](https://gitlab.com/mantik-ai/tutorials/issues).
* Make sure your issue **is really a bug**, and is not a support request.
* If you find a **closed** issue that seems like it is the same thing that you're experiencing, open a new issue and include a link to the original issue in the body of your new one.

Submit your bugs in the [issue tracker](https://gitlab.com/mantik-ai/tutorials/issues):

1. Use the bug issue template. This can be selected from the dropdown menu in the `Description` field.
2. Completely fill out the template.
3. Save the issue.

### Suggesting improvements

You may submit enhancement suggestion such as new examples or improvements of existing examples by submitting a ticket.
Before submitting a new suggestion, check that your **issue does not already exist** in the [issue tracker](https://gitlab.com/mantik-ai/tutorials/issues).

## Code contributions

### Adding an example

When adding a new example, checkout a new branch locally, push it to the remote repository and open a merge request.

Add a `README.md` to your example folder:

* Reference the authors of the example.
* Indicate for which version of Mantik the example is valid.
* Give a short description of your application (scope, data, etc.).
* List any required software prerequisites.

### Merge requests

* Describe your changes as accurately as possible.
  The merge request body should be kept up to date as it will usually form the base for the changelog entry.
* Be sure that your merge request contains tests that cover the changed or added code.
  Tests are generally required for code be to be considered mergable, and code without passing tests will not be merged.
  Ideally, the test coverage should not decrease.
* Ensure your merge request passes the pre-commit checks.
  Remember that you can run these tools locally.
* If your changes warrant a documentation change, the merge request must also update the documentation.

**Note:**
Make sure your branch is [rebased](https://docs.github.com/en/get-started/using-git/about-git-rebase) against the latest
main branch.

## Contributors

Elia Boscaini, Fabian Emmerich, Enxhi Kreshpa, Thomas Seidler
