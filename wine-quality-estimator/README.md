# Mantik Demo: Wine Quality Estimator

This demo enables you to execute training of a linear model to predict wine
quality from wine-related input features such as acidity. It follows
[this example provided by mlflow](https://github.com/mlflow/mlflow/tree/master/examples/docker)
and is adjusted to work on the Mantik platform.

The data set used in this example is from
http://archive.ics.uci.edu/ml/datasets/Wine+Quality (P. Cortez, A. Cerdeira, F. Almeida, T. Matos and J. Reis.
Modeling wine preferences by data mining from physicochemical properties.
In Decision Support Systems, Elsevier, 47(4):547-553, 2009).

> **Note:**
> You can follow this tutorial on the mantik docs [here](https://mantik-ai.gitlab.io/mantik/tutorials/mantik-on-juwels/tutorial-outline.html).

## Prerequisites

You will need:
 - a JuDoor account and access to a compute project on JUWELS, and
 - a Mantik user account (register [here](https://cloud.mantik.ai/register)).

**Note:** Throughout this demo it is assumed that the Apptainer image for execution
is already present as `mlproject/wine-quality-executor.sif` or can be built locally, which requires an installation of Apptainer. 

**Note 2:** In this demo we use Apptainer images since it was tested with
`apptainer version 1.1.4`. However, it should also work for newer versions
and `singularity` images.

## Build the required Apptainer image

For this demo project we provide an Apptainer (Singularity) definition file
(`mlprojcet/recipe.def`).

Build the image as follows:

```commandline
apptainer build mlproject/wine-quality-executor.sif mlproject/recipe.def
```

**Note:**
 - Building with Apptainer might require sudo.
 - If you have Singularity installed, you can just replace `apptainer` with
`singularity`.
 - We also include a Dockerfile for completeness' sake. One requires either a Dockerfile *or* an Apptainer
recipe to build the image as Apptainer images can be
built from `docker` images, see 
[the containers tutorial](https://mantik-ai.gitlab.io/mantik/remote-execution/containerization.html).

## Setup the environment

Credentials for user authentication as well as the accounting project are read
from the environment variables `MANTIK_UNICORE_USER`,
`MANTIK_UNICORE_PASSWORD`, and `MANTIK_COMPUTE_BUDGET_ACCOUNT`, respectively.
These need to be set in the execution environment:

```commandline
export MANTIK_UNICORE_USERNAME=<user>
export MANTIK_UNICORE_PASSWORD=<pasword>
export MANTIK_COMPUTE_BUDGET_ACCOUNT=<account charged for consumed compute resources>
```

Additionally, the information on where to send mlflow logs to is required:

```commandline
export MLFLOW_TRACKING_URI=<uri>
```

You can just use the URL of the mantik platform landing page - the mantik client
will take care of rerouting to the API.

For access to the mantik platform, you will need to supply credentials via:

```commandline
export MANTIK_USERNAME=<user>
export MANTIK_PASSWORD=<password>
```

The file `environment.sh` can be used to set all the environment variables:
 - Replace the placeholders for the values by the actual values.
 - Run `source environment.sh` in a shell before calling the python script
from the same shell.

### The backend configuration file

The backend config is in YAML format (json is also supported) and may contain information for the
resources that are allocated for the job.

We provide an example backend configuration in `mlproject/compute-backend-config.yaml`.

For documentation on the settings, see 
[the user guide](https://mantik-ai.gitlab.io/mantik/remote-execution/compute-backend-config.html).

### Running the demo

The demo can be run by invoking `python run.py`.

### Dependency management

This project needs the python packages `mantik` and `scikit-learn`. Both are
installed in the `Dockerfile` and `recipe.def`. Versions are pinned.

In the case of more dependencies we recommend creating a `requirements.txt`
file or the usage of [`poetry`](https://python-poetry.org/) for dependency
management.


### Data

In accordance to MLflow Docker backend, where the MLproject directory is
mounted, all files in this directory are transferred alongside the Apptainer
image and accessible at runtime.

Make sure not to have unnecessary files in that directory in order not to slow
down the file upload. We recommend not to build data into Apptainer
images and ideally store them directly on the remote system and just reference
the respective path in your code or make the data path a parameter in your MLproject
definition.


### Results

Execution is asynchronous, i.e. the local process terminates as soon as all
data are transferred and the Job is submitted. However, data transfer might
take a while since Apptainer images used here are large.

Results can then be checked in the mlflow UI hosted on the mantik platform.
