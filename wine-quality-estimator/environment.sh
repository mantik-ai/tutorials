#!/bin/sh

export MANTIK_USERNAME=<user>
export MANTIK_PASSWORD=<password>

export MLFLOW_TRACKING_URI=<tracking uri>

export MANTIK_UNICORE_USERNAME=<unicore user>
export MANTIK_UNICORE_PASSWORD=<unicore password>
export MANTIK_COMPUTE_BUDGET_ACCOUNT=<account charged for consumed compute resources>

