# Mantik tutorials

This is a collection of tutorials and demos for usage of the mantik platform.

Project `mantik-minimal-project` is a minimal "Hello World"-like Mantik project, where the only executed command is printing the user input. 
With this project you can get an idea of how the MLproject and the Compute Backend Config files work.

Project `wine-quality-estimator` provides an introduction to tracking machine learning runs with Mantik as well as submitting runs on a compute backend from the console.

Project `cats-and-dogs` showcases additional features like using `venv` to manage dependencies (or alternatively Apptainer). You will train a `Tensorflow` model on Juwels to classify pictures of animals as cats or dogs. Progress and Figures are tracked to the Mantik UI and jobs submitted via the Mantik CLI.

Additionally, `hazard_mapping`, `seasonal_contrast` and `asos` are demos provided by the [K1:STE](https://kiste-project.de/) project that showcase the adaption of research applications to the Mantik platform. They require access to the respective repositories hosted on [gitlab](https://gitlab.jsc.fz-juelich.de/) of the Juelich Supercomputing Center.
*Note*, they are based on an outdated Mantik version 0.1.1. Therefore, they require updating before submitting the job to the compute backend.

## The mantik platform

Mantik offers a web based platform for managing you ML experiments. 

It can be reached via
[cloud.mantik.ai](https://cloud.mantik.ai).

## Getting started
Register [here](https://cloud.mantik.ai/register).

Consult the [Mantik documentation](https://mantik-ai.gitlab.io/mantik/)'s _Quickstarts_ and _Tutorials_ on how to get started.

## Bug Reports

See [section _Reporting Bugs_ in CONTRIBUTING](CONTRIBUTING.md).

## Platform Admins
[@thomas_ambrosys](https://gitlab.com/thomas_ambrosys) and
[@fabian.emmerich](https://gitlab.com/fabian.emmerich)