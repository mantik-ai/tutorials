# Mantik Demo: Cats and dogs classification

This demo enables you to execute training of a deep neural network with tensorflow to classify dog and cat images. It is based on a larger dataset (~ 1GB) compared to [`wine-quality-estimator`](https://gitlab.com/mantik-ai/tutorials/-/tree/main/wine-quality-estimator) and runs on GPUs, which adds complexity to the setup of the environment. Our training setup is based on this [tensorflow tutorial](https://www.tensorflow.org/datasets/keras_example) and the model is taken from [here](https://towardsdatascience.com/recognizing-cats-and-dogs-with-tensorflow-105eb56da35f).

The data set used in this example is from https://www.tensorflow.org/datasets/catalog/cats_vs_dogs (Elson, Jeremy and Douceur, John (JD) and Howell, Jon and Saul, Jared. Asirra: A CAPTCHA that Exploits Interest-Aligned Manual Image Categorization. In Proceedings of 14th ACM Conference on Computer and Communications Security (CCS), Association for Computing Machinery, Inc., 2017).


## Prerequisites

You will need:
 - a JuDoor account and access to a compute project on JUWELS, and
 - a Mantik user account (register [here](https://cloud.mantik.ai/register)).

In this tutorial, we provide two options to create our environment to install our dependencies:

1. Build an Apptainer image and run your job in the created image, which allows full control of the environment including operating system etc.
2. Create a [`venv`](https://docs.python.org/3/library/venv.html) to install python packages. This option provides the possibilty to load modules already installed on the computing cluster.

### (Option 1) Build the required Apptainer image

We base our image on the official tensorflow Docker image from the [Docker hub](https://hub.docker.com/r/tensorflow/tensorflow/tags), which provides a plethora of different tensorflow versions including GPU computing capabilities.

For this demo project we provide an Apptainer (Singularity) definition file
(`mlprojcet/recipe.def`).

Build the image as follows:

```commandline
apptainer build mlproject/cats-and-dogs-executor.sif mlproject/recipe.def
```

**Note:**
 - Building with Apptainer might require sudo.
 - If you have Singularity installed, you can just replace `apptainer` with `singularity`.
 - We also include a Dockerfile for completeness' sake. One requires either a Dockerfile *or* an Apptainer
recipe to build the image as Apptainer images can be
built from `docker` images, see
[the containers tutorial](https://mantik-ai.gitlab.io/mantik/remote-execution/containerization.html).

**Note 2:** In this demo we use Apptainer images since it was tested with
`apptainer version 1.1.4`. However, it should also work for newer versions
and Singularity images.

### (Option 2) Build the venv environment on JUWELS (JSC)
*Note, this was tested with Python version 3.10.4 and pip version 23.0.1*

*Note2, you need to install the venv on a JUWELS Booster (JUWELS Cluster) login node when running it on JUWELS Booster (JUWELS Cluster).*

We will create our virtual environment on the cluster with a tensorflow version provided by `modules` that is compatible with Cuda gpu drivers installed on the cluster. We will use `TensorFlow/2.11.0-CUDA-11.7`, which is part of the current stage `Stages/2023`. To load this model and its required dependencies, we call

```bash
module load Stages/2023
module load GCC/11.3.0
module load OpenMPI/4.1.4
module load TensorFlow/2.11.0-CUDA-11.7
```

Checking your currently used python exectuable, should return the following.

```bash
which python
```

```bash
/p/software/juwelsbooster/stages/2023/software/Python/3.10.4-GCCcore-11.3.0/bin/python
```

If this is not the case, make sure you are not overwriting Python with another enviroment management tool like for example `conda`.

We will then later specify these modules in our Compute Backend config file to reproduce the same environment when running our job (see below).

Let us now create our environment in folder `cats_and_dogs_env`.
```bash
python -m venv cats_and_dogs_env
```

and activate the environment

```bash
. cats_and_dogs_env/bin/activate
```

(use `deactivate`, if you want to deactivate the environment)

Prefereably, use the latest pip version by running

```bash
pip install --upgrade pip
```

Now, we can use pip to install our python packages. We only install packages not included in the previously loaded modules. The additional required packages are given as a `requirements.txt` file.

```bash
pip install -r requirements.txt
```

## Setup the environment variables


Credentials for user authentication as well as the accounting project are read
from the environment variables `MANTIK_UNICORE_USER`,
`MANTIK_UNICORE_PASSWORD`, and `MANTIK_COMPUTE_BUDGET_ACCOUNT`, respectively.
These need to be set in the execution environment:

```commandline
export MANTIK_UNICORE_USERNAME=<user>
export MANTIK_UNICORE_PASSWORD=<pasword>
export MANTIK_COMPUTE_BUDGET_ACCOUNT=<account charged for consumed compute resources>
```

Additionally, the information on where to send MLflow logs to is required:

```commandline
export MLFLOW_TRACKING_URI=<uri>
```

You can just use the URL of the mantik platform landing page - the mantik client
will take care of rerouting to the API.

For access to the mantik platform, you will need to supply credentials via:

```commandline
export MANTIK_USERNAME=<user>
export MANTIK_PASSWORD=<password>
```

The file `environment.sh` can be used to set all the environment variables:
 - Replace the placeholders for the values by the actual values.
 - Run `source environment.sh` in a shell before calling the python script
from the same shell.

### The backend configuration file

The backend config is in YAML format (json is also supported) and may contain information for the
resources that are allocated for the job.

We provide an example backend configuration in `mlproject/compute-backend-config-apptainer.yaml`.

For documentation on the settings, see
[the user guide](https://mantik-ai.gitlab.io/mantik/remote-execution/compute-backend-config.html).

## Running the demo

### (Option 1) Submitting the job running in an Apptainer image

We use the Mantik API to submit our training script as a job on JUWELS on JSC. We will first run our job in the constructed Apptainer image (see below for running the same job in a `venv` environment).

For this, we will use the Compute Backend config `mlproject/compute-backend-config-apptainer.yaml`, which gives an absolute path to our Apptainer image stored on the cluster. While Mantik provides the option to upload images directly to the cluster, transferring such large files through Mantik's compute backend is highly discouraged and may likely fail.

We therefore **need** to upload our image to JUWELS Booster to one of our folders and **specify** it as the new `Path`. In addition, we specify files to `exclude` that we do not want to upload to the cluster.

Similarly, our training data is around `1 Gb`, which we do not want to upload/download for every run. Therefore you need to **change** `data_dir` in the `MLproject` file to an absolute path to a folder you own. The dataset is hosted on `Keras`, which will take care of the rest.

We can then submit our job via

```bash
mantik runs submit mlproject/MLproject \
  --run-name <Name of the Run> \
  --connection-id <Mantik Connection UUID> \
  --entry-point main \
  --backend-config mlproject/compute-backend-config-apptainer.yaml \
  --project-id <ID of the project to which the run should be linked> \
  --experiment-repository-id <ID of the experiment repository to which the run should be linked> \
  --code-repository-id <ID of the code repository where the mlproject is located> \
  --branch <Name of the code repository's branch> \
  --compute-budget-account <Name of the compute budget account on HPC> \
  -P <key>=<value> \
  -P <key>=<value>
```

```bash
{"runId":"25d2dad9-dfe2-4971-ad95-b3a9dc363700","timestamp":"2023-03-29T16:39:09+020"}
```

where `runId` is the ID of the submitted run. 

### (Option 2) Submitting the job running in a `venv` environment

To handle python dependencies with `venv`, we created a venv environment on the cluster with a python executable available through modules (see `Prerequisite Option 2`).

We need to specify the folder containing our venv environment in our Compute backend config file `compute-backend-config-venv.yaml`. Here, we also specify the modules required to run our script. For now, all modules are included that we need to use tensorflow in our environment. The tutorial assumes that we created our venv environment on a JUWELS Booster login node. Therefore, we need to specify a queue on the JUWELS Booster, i.e. `develbooster`.

**Update** the `Python` variable in `compute-backend-config-venv.yaml` to the absolute path to your venv environment created above. In addition, we specify files to `exclude` that we do not want to upload to the cluster.

Our dataset is provided from keras and has a size of `1 Gb`, which we do not want to upload/download for every run. Therefore you need to **change** `data_dir` in the `MLproject` file to an absolute path to a folder you own. The dataset is hosted on `Keras`, which will take care of the rest.

We can then submit our job (analogously to `Option 1`) via

```bash
mantik runs submit mlproject/MLproject \
  --run-name <Name of the Run> \
  --connection-id <Mantik Connection UUID> \
  --entry-point main \
  --backend-config mlproject/compute-backend-config-venv.yaml \
  --project-id <ID of the project to which the run should be linked> \
  --experiment-repository-id <ID of the experiment repository to which the run should be linked> \
  --code-repository-id <ID of the code repository where the mlproject is located> \
  --branch <Name of the code repository's branch> \
  --compute-budget-account <Name of the compute budget account on HPC> \
  -P <key>=<value> \
  -P <key>=<value>
```

To simplify the process of submitting a run without repeatedly specifying project ID, experiment repository ID, etc., as options, you can either use the re-run feature of the respective run on the platform (`Project > Runs > Submissions > Re-run button in the "Actions" tab`), or store these IDs in environment variables: `MANTIK_PROJECT_ID`, `MANTIK_EXPERIMENT_REPOSITORY_ID`, `MANTIK_CODE_REPOSITORY_ID`, and `MANTIK_DATA_REPOSITORY_ID`.


```bash
{"runId":"25d2dad9-dfe2-4971-ad95-b3a9dc363700","timestamp":"2023-03-29T16:39:09+020"}
```

### Checking in on our jobs with the Mantik UI

To access and obtain information regarding a submitted run, navigate to `Project > Runs > Submissions`. Here you will find an overview of all the runs related to a specific project and details such as the 
experiment repository, the connection used, the run's start time, and its current status. You can interact with a
run by clicking on any of the icons in the "Action" column.

## Using the MLflow UI to check results

Visit the [MLflow UI](https://cloud.mantik.ai/mlflow/) hosted by Mantik. Find your run by clicking on the experiment you are tracking to `experiment_name` in `train.py`. You should be able to identify the run by looking at the time of creation `Created`. Click on blue highlighted time of creation to get more info on your run.

First, open the dropdown menu `Metrics` and click on `train_AUC`. which should show you the AUC as an evolution of time. You can also click on the field `Y-axis` to add `valid_AUC` to the same graph. Changing X-axis to `Step` aligns the metrices to the same point of reference.

In addition, in the experiment view, we can select multiple runs (through the checkbox on the left) and compare final test metrics for example as a function of different hyperparameters (e.g. batch_size).

## Modify parameters

We can directly specify keyword arguments when submitting our job, with the parameter option `-P <key>=<value>`

For example, reducing the batch size from the default to 128 can be done by calling

```bash
mantik runs submit mlproject/MLproject \
  --run-name <Name of the Run> \
  --connection-id <Mantik Connection UUID> \
  --entry-point main \
  --backend-config mlproject/compute-backend-config-venv.yaml \
  --project-id <ID of the project to which the run should be linked> \
  --experiment-repository-id <ID of the experiment repository to which the run should be linked> \
  --code-repository-id <ID of the code repository where the mlproject is located> \
  --branch <Name of the code repository's branch> \
  --compute-budget-account <Name of the compute budget account on HPC> \
  -P batch_size=16
```

Or assigning our run to a different experiment

```bash
mantik runs submit mlproject/MLproject \
  --run-name <Name of the Run> \
  --connection-id <Mantik Connection UUID> \
  --entry-point main \
  --backend-config mlproject/compute-backend-config-venv.yaml \
  --project-id <ID of the project to which the run should be linked> \
  --experiment-repository-id <ID of the experiment repository to which the run should be linked> \
  --code-repository-id <ID of the code repository where the mlproject is located> \
  --branch <Name of the code repository's branch> \
  --compute-budget-account <Name of the compute budget account on HPC> \
  -P <key>=<value> \
  -P <key>=<value> 
  -P experiment_name=test_cats
```

### Results

Execution is asynchronous, i.e. the local process terminates as soon as all
data are transferred and the Job is submitted. However, data transfer might
take a while since Apptainer images used here are large.

Results can then be checked in the MLflow UI hosted on the mantik platform.
