# The data set used in this example is from
# http://archive.ics.uci.edu/ml/datasets/Wine+Quality
# P. Cortez, A. Cerdeira, F. Almeida, T. Matos and J. Reis.
# Modeling wine preferences by data mining from physicochemical properties.
# In Decision Support Systems, Elsevier, 47(4):547-553, 2009.
import argparse
import logging
import os
import pathlib

import keras
import mantik
import matplotlib as mpl
import matplotlib.pyplot as plt
import mlflow
import numpy as np
import sklearn.metrics
import tensorflow as tf
import tensorflow_datasets


logging.basicConfig(
    format="%(asctime)s.%(msecs)03d %(levelname)-8s %(message)s",
    level=logging.INFO,
    datefmt="%Y-%m-%d %H:%M:%S",
)
mpl.use("Agg")


def get_model(args):
    model = keras.Sequential(
        [
            keras.layers.Conv2D(16, (3, 3), activation="relu", input_shape=(*args.resize_image_shape, 3)),
            keras.layers.MaxPool2D((2, 2)),
            keras.layers.Conv2D(32, (3, 3), activation="relu"),
            keras.layers.MaxPool2D((2, 2)),
            keras.layers.Conv2D(64, (3, 3), activation="relu"),
            keras.layers.MaxPool2D((2, 2)),
            keras.layers.Flatten(),
            keras.layers.Dense(512, activation="relu"),
            keras.layers.Dense(1, activation="sigmoid"),
        ]
    )
    return model


def log_best_epoch(history):
    if history is None:
        return
    hist = history.history["val_loss"]
    n_epochs_best = np.argmin(hist)
    mlflow.log_param("n_epochs_best", n_epochs_best)


def save_log_figure(figure, filename, track_figures=True):
    figure.savefig(f"{filename}")
    if track_figures:
        mlflow.log_artifact(f"{filename}")


def plot_sample(ds_tf, ds_info, filename, track_figures):
    fig = tensorflow_datasets.show_examples(ds_tf, ds_info)
    save_log_figure(fig, filename, track_figures=track_figures)


def preprocess(
    ds_tf, ds_info, folder_figures, batch_size=128, train=True, resize_image_shape=(100, 100), track_figures=True
):
    def normalize_img(image, label):
        """Normalizes images: `uint8` -> `float32`."""
        return tf.cast(image, tf.float32) / 255.0, label

    def resize_img(image, label):
        image = tf.image.resize(
            image,
            resize_image_shape,
        )
        return image, label

    def cast_label(image, label):
        """Cast label to float, required for f1-score computation but brakes plotting function!"""
        return image, tf.cast(label, tf.float32)

    ds_tf = ds_tf.map(normalize_img, num_parallel_calls=tf.data.AUTOTUNE)
    ds_tf = ds_tf.map(resize_img, num_parallel_calls=tf.data.AUTOTUNE)
    plot_sample(
        ds_tf,
        ds_info,
        folder_figures / f"{'train' if train else 'test'}_samples_preprocessed.pdf",
        track_figures=track_figures,
    )
    ds_tf = ds_tf.map(cast_label, num_parallel_calls=tf.data.AUTOTUNE)
    if train:
        ds_tf = ds_tf.cache()
        ds_tf_size = ds_tf.__len__().numpy()
        ds_tf = ds_tf.shuffle(ds_tf_size)
        ds_tf = ds_tf.batch(batch_size)
    else:
        ds_tf = ds_tf.batch(batch_size)
        ds_tf = ds_tf.cache()
    ds_tf = ds_tf.prefetch(tf.data.AUTOTUNE)
    return ds_tf


def plot_confusion_matrix(true, predictions, folder_figures):
    cm = sklearn.metrics.confusion_matrix(true, predictions, labels=[0, 1])
    disp = sklearn.metrics.ConfusionMatrixDisplay(confusion_matrix=cm, display_labels=["cats", "dogs"])
    disp.plot()
    fig = disp.figure_
    save_log_figure(fig, folder_figures / "confusion_matrix.pdf")


def plot_auc_curve(true, predictions, folder_figures):
    fpr, tpr, thresholds = sklearn.metrics.roc_curve(true, predictions)
    fig, ax = plt.subplots()
    auc_rf = sklearn.metrics.auc(fpr, tpr)
    ax.plot(fpr, tpr, label=f"area = {auc_rf:.3f}")
    ax.set_xlabel("False positive rate")
    ax.set_ylabel("True positive rate")
    ax.legend(loc="best")
    save_log_figure(fig, folder_figures / "auc_curve.pdf")


def evaluate_model(model, ds_test, folder_figures):
    prediction_probabilities = model.predict(ds_test)
    true = [x[1] for x in ds_test.unbatch().as_numpy_iterator()]
    predictions = prediction_probabilities.round()
    metrics_test = model.evaluate(ds_test)
    mlflow.log_metrics({"test_loss": metrics_test[0], "test_AUC": metrics_test[1]})
    plot_confusion_matrix(true, predictions, folder_figures)
    plot_auc_curve(true, prediction_probabilities, folder_figures)


def create_mlflow_experiment(name):
    experiment_id = mlflow.get_experiment_by_name(name)
    if not experiment_id:
        experiment_id = mlflow.create_experiment(name)
    return experiment_id


class MetricsTracker(keras.callbacks.Callback):
    def __init__(self) -> None:
        super().__init__()
        self.total_batches_trained = None

    def on_train_begin(self, logs=None):
        self.total_batches_trained = 0

    def on_train_batch_begin(self, epoch, logs=None):
        self.total_batches_trained += 1

    def on_train_batch_end(self, batch, logs=None):
        metrics = {"train_loss": logs["loss"], "train_AUC": logs["AUC"]}
        logging.info(f"on_train_batch_end: step:{self.total_batches_trained}, {batch=}, {metrics=}, {logs=}")
        mlflow.log_metrics(metrics, step=self.total_batches_trained)

    def on_test_end(self, logs=None):
        metrics = {"valid_loss": logs["loss"], "valid_AUC": logs["AUC"]}
        logging.info(f"on_test_end: step:{self.total_batches_trained}, {metrics=}, {logs=}")
        mlflow.log_metrics(metrics, step=self.total_batches_trained)


def train_model(args, ds_train, ds_valid):
    model = get_model(args)

    loss = tf.keras.losses.BinaryCrossentropy(
        from_logits=False, label_smoothing=0.0, axis=-1, name="binary_crossentropy"
    )

    AUC = tf.keras.metrics.AUC(
        num_thresholds=200,
        curve="ROC",
        name="AUC",
    )

    early_stopping = tf.keras.callbacks.EarlyStopping(
        monitor="val_loss",
        min_delta=0,
        patience=2,
        restore_best_weights=True,
    )

    model.compile(optimizer="adam", loss=loss, metrics=[AUC])
    logging.info(model.summary())
    history = model.fit(
        ds_train,
        validation_data=ds_valid,
        epochs=args.epochs,
        callbacks=[MetricsTracker(), early_stopping],
    )
    model.save("saved_model/my_model.keras")
    return model, history


def main(args):
    if args.local:
        # locally; command required to log to Mantik MLflow UI when executing
        # on remote; already automatically executed
        mantik.init_tracking()
    create_mlflow_experiment(args.experiment_name)
    mlflow.set_experiment(args.experiment_name)
    if args.debug:
        args.batch_size = 16
        args.epochs = 2
        args.run_name = "debug"
    os.makedirs(args.folder_figures, exist_ok=True)
    args.folder_figures = pathlib.Path(args.folder_figures)
    # Initialize new run
    with mlflow.start_run(run_name=args.run_name):
        mlflow.log_params(args.__dict__)
        if args.debug:
            split = ["train[:1%]", "train[1%:2%]", "train[2%:3%]"]
        else:
            split = ["train[:70%]", "train[70%:85%]", "train[85%:]"]
        (ds_train, ds_valid, ds_test), ds_info = tensorflow_datasets.load(
            "cats_vs_dogs",
            split=split,
            shuffle_files=True,
            as_supervised=True,
            with_info=True,
            data_dir=args.data_dir,
        )

        plot_sample(ds_train, ds_info, args.folder_figures / "train_samples_raw.pdf", track_figures=True)
        plot_sample(
            ds_test, ds_info, args.folder_figures / "test_samples_raw.pdf", track_figures=args.track_all_figures_data
        )
        plot_sample(
            ds_valid, ds_info, args.folder_figures / "valid_samples_raw.pdf", track_figures=args.track_all_figures_data
        )

        ds_train = preprocess(
            ds_train,
            ds_info,
            folder_figures=args.folder_figures,
            batch_size=args.batch_size,
            train=True,
            resize_image_shape=args.resize_image_shape,
            track_figures=True,
        )
        ds_test = preprocess(
            ds_test,
            ds_info,
            folder_figures=args.folder_figures,
            batch_size=args.batch_size,
            train=False,
            resize_image_shape=args.resize_image_shape,
            track_figures=args.track_all_figures_data,
        )
        ds_valid = preprocess(
            ds_valid,
            ds_info,
            folder_figures=args.folder_figures,
            batch_size=args.batch_size,
            train=False,
            resize_image_shape=args.resize_image_shape,
            track_figures=args.track_all_figures_data,
        )

        if args.load_trained_model:
            history = None
            model = tf.keras.models.load_model(args.load_trained_model)
        else:
            model, history = train_model(args, ds_train, ds_valid)

        evaluate_model(model, ds_test, folder_figures=args.folder_figures)

        log_best_epoch(history)

        return ds_train, ds_valid, ds_test, history


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--batch_size",
        type=int,
        default=128,
        help="Batch size for training and evaluation.",
    )
    parser.add_argument("--epochs", type=float, default=40, help="Epochs to train the model for.")
    parser.add_argument("--folder_figures", type=str, default="figures/", help="Folder where figures saved.")
    parser.add_argument(
        "--data_dir",
        type=str,
        default=None,
        help="Location where data stored/downloaded to.",
    )
    parser.add_argument(
        "--experiment_name",
        type=str,
        default="cats_and_dogs",
        help="Name of mlflow experiment.",
    )
    parser.add_argument("--run_name", type=str, default="tutorial", help="Name of individual run.")
    parser.add_argument(
        "--resize_image_shape",
        type=tuple,
        default=(100, 100),
        help="Image resized to this shape. Note, shape of image includes 3 additional color channels.",
    )
    parser.add_argument(
        "--debug",
        action="store_true",
        help="Reduces size of dataset and batch_size for quick debugging.",
    )
    parser.add_argument(
        "--load_trained_model",
        type=str,
        default=None,
        help="Will load and evaluate this model only.",
    )
    parser.add_argument(
        "--track_all_figures_data",
        type=bool,
        default=False,
        help="Will upload saved figures to Mantik tracking server.",
    )
    parser.add_argument(
        "--local",
        action="store_true",
        help="Required if want to execute locally.",
    )
    args = parser.parse_args()
    ds_train, ds_valid, ds_test, history = main(args)
